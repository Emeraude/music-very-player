#!/usr/bin/env node

const Discord  = require('discord.js')

const { GuildsManager } = require ('./src/guilds_manager')

const client = new Discord.Client()
const guild_manager = new GuildsManager()

let index = 0;
console.time("ready")
client.once('ready', () => {
  console.timeEnd("ready")
  console.log("= MVP BOT IS READY =")
  client.on('message', message => {
    const current_index = index++;
    console.log("> NEW MESSAGE", current_index)
    try {
      guild_manager.handle_message(message)
    } catch (err) {
      console.error('> catch err', err)
    }
    console.log("> FINISH", current_index)
  })
})

client.login(process.env['DISCORD_API_KEY'])
