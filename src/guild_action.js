/**
 * @class
 * @desc A class that describe an action, used each time an event matches a pattern described in the configuration file
 * @property {Object} message The complete message. See discord.js documentation for mor informations
 * @property {string} target
 * @property {string} action
 * @property {string[]} params The parameters for the action
 */
class GuildAction {
  /**
   * @constructor
   * @desc Create a new GuildAction, setting its attributes to the given values
   * @param {Object} parameters
   * @param {Object} parameters.message
   * @param {string} parameters.target
   * @param {string} parameters.action
   * @param {string[]} parameters.params
   */
  constructor({ target, action, message, params }) {
    this.target = target;
    this.action = action;
    this.message = message;
    this.params = params;
  }
}

module.exports = { GuildAction }
