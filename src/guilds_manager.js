const { InputManager } = require ('./input_manager')
const { ResourceManager } = require('./resource_manager')
const { PlaylistsManager } = require('./playlists_manager')
const { PlayerManager } = require('./player_manager')
const config = require('../config.json')

/**
 * This class handles each guilds independantly.
 * In order to do so, it accept every message in {@link handle_message}
 * and redirect the action to execute to other Manager dedicated to only one
 * {@link Discord.Guild}.
 *
 * @class
 */
class GuildsManager {
  #guilds

  constructor() {
    this.#guilds = {}
  }

  toString() {
    return "GuildsManager#" + Object.keys(this.#guilds).map(g => `[guid:${g}]`)
  }

  get_manager(guild, name) {
    if (guild?.guild?.id) guild = guild.guild.id // we may receive a message instead of a guid.id
    return this.#guilds[guild][name];
  }

  /**
   *
   * @param {Discord.Message} message
   */
  async handle_message(message) {
    const guild_id = message.guild.id
    if (!this.#guilds[guild_id]) {
      const current_config = config[message.guild.id] || config['default']
      this.#guilds[guild_id] = {
        input: new InputManager(this, guild_id, current_config),
        player: new PlayerManager(this, guild_id, current_config),
        resource: new ResourceManager(this, guild_id, current_config),
        playlists: await new PlaylistsManager(this, guild_id, current_config),
        config: current_config,
      }
    }
    const guild_action = await this.#guilds[guild_id].input.handle_message(message)
    if (guild_action) {
      if (!this.#guilds[guild_id]?.[guild_action.target]?.[guild_action.action]) {
        message.channel.send(`The command ${guild_action.target} ${guild_action.action} is not implemented yet`)
      } else {
        try {
          const output = await this.#guilds[guild_id][guild_action.target][guild_action.action](guild_action)
          return output
        } catch (e) {
          const error_message = await message.channel.send(":warning: Something went wrong. React with :eyes: to get more information about this error.")
          error_message.react("👀")
          error_message.awaitReactions((r, u) => r.emoji.name === "👀" && u.id !== error_message.author.id, {max: 1})
            .then(() => {
              message.channel.send(`Target manager: **${guild_action.target}** Action: **${guild_action.action}** Parameters: **${guild_action.params.join(', ')}**`
                                   + "\n"
                                   + `Original message: \`${guild_action.message}\``
                                   + "\n"
                                   + "```js\n"
                                   + e.stack
                                   + "\n```")
            })
          return null
        }
      }
    }
  }
}

module.exports = { GuildsManager }
