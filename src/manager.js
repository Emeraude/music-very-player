/**
 * An abstract class that is meant to be extended by actual managers.<br />
 * Managers are classes that have actions, which are used by the {@link GuildsManager} according to the configuration file.
 */
class Manager {
  #gmanager
  #guild_id

  /**
   * @param {GuildsManager} gmanager An instance of GuildsManager
   * @param {string} guild_id The id of the Discord guild this managers is working on
   */
  constructor(gmanager, guild_id) {
    this.#gmanager = gmanager
    this.#guild_id = guild_id
  }

  /**
   * Retrieves another manager related to the same Discord guild
   * @param {"input"|"player"|"playlist"|"resource"} name The name of the manager to retrieve
   * @returns {InputManager|PlayerManager|PlaylistsManager|ResourceManager} The associated manager instance
   */
  get_manager(name) {
    return this.#gmanager.get_manager(this.#guild_id, name)
  }
}

module.exports = { Manager }
