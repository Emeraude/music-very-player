/**
 * @param {durationString}
 * @returns {number}
 */
function from_string_to_seconds(str = "") {
  return str
    .split(':')
    .map(v => parseInt(v))
    .reduce((acc, v) => acc * 60 + v)
}

/**
 * @typedef {string} durationString
 * @desc A string representation of the duration, following the [[HH:]mm:]ss format.<br />
 * This format is the same as the one used by YouTube
 */

/**
 * @param {number}
 * @returns {durationString}
 */

function from_seconds_to_string(seconds) {
  const secs = parseInt(seconds % 60)
  const minutes = parseInt((seconds / 60) % 60)
  const hours = parseInt(seconds / 3600)
  return ''
    + (hours > 0 ? hours + ':' : '')
    + String(minutes).padStart((hours > 0 ? 2 : 1), '0')
    + ':'
    + String(secs).padStart(2, '0')
}

module.exports = { from_string_to_seconds, from_seconds_to_string }
