const { Youtube } = require('../src/youtube');

describe('playlist basic manipulations', () => {
  const youtube = new Youtube();
  test('search_track', async () => {
    const tracks = await youtube.search_track("FROOT: Full Instrumental Album (MARINA AND THE DIAMONDS)")
    expect(tracks.length).not.toEqual(0)
    expect(tracks[0].get_url()).toEqual('https://www.youtube.com/watch?v=0WvTtIfX04c')
    expect(tracks[0].get_title()).toEqual('FROOT: Full Instrumental Album (MARINA AND THE DIAMONDS)')
    expect(tracks[0].get_length_as_string()).toEqual('48:27')
    expect(tracks[0].get_source()).toEqual('youtube')
  })

  test('get_playlist', async () => {
    const playlist = await youtube.get_playlist("https://www.youtube.com/playlist?list=PLtKVsbX6mq9RvO__n08wujegVI0NsuNiu")
    const track = playlist.get_current_track()
    expect(playlist.get_length()).toEqual(61)
    expect(track.get_url()).toEqual('https://www.youtube.com/watch?v=jfAbX-Fg9N0')
    expect(track.get_title()).toEqual('Attack on Titan: Original Soundtrack I - attack ON titan | High Quality | Hiroyuki Sawano')
    expect(track.get_length_as_string()).toEqual('4:20')
    expect(track.get_source()).toEqual('youtube')
  })

  test('get_metadata', async () => {
    const track = await youtube.get_metadata("https://www.youtube.com/watch?v=0WvTtIfX04c")
    expect(track.get_url()).toEqual('https://www.youtube.com/watch?v=0WvTtIfX04c')
    expect(track.get_title()).toEqual('FROOT: Full Instrumental Album (MARINA AND THE DIAMONDS)')
    expect(track.get_length_as_string()).toEqual('48:27')
    expect(track.get_source()).toEqual('youtube')
  })
})
