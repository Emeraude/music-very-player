const { Playlist } = require('../src/playlist');

describe('playlist basic manipulations', () => {
  const p1 = new Playlist();
  const p2 = new Playlist({ tracks: [{ title: "HomeoJuliette", url: "https://www.youtube.com/watch?v=WLXCTvMtY5Y" }] });
  test('initial state', () => {
    expect(p1.get_current_track()).toBeNull()
  })
  test('add new track', () => {
    expect(p1.push("track1")).toBeTruthy()
    expect(p1.get_current_track()).toEqual("track1")
  })
  test('remove inexisting track', () => {
    expect(p1.delete(100)).toBeFalsy()
    expect(p1.get_current_track()).toEqual("track1")
  })
  test('remove existing track', () => {
    expect(p1.delete(0)).toBeTruthy()
    expect(p1.get_current_track()).toBeNull()
  })

  test('initial state 2', () => {
    expect(p2.get_current_track()).not.toBeNull()
    expect(p2.get_current_track().get_title()).toEqual("HomeoJuliette")
  })
})
