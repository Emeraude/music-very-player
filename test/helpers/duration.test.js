const { from_string_to_seconds, from_seconds_to_string } = require('../../src/helpers/duration')

describe('from_string_to_seconds', () => {
  test('1 second long, various formats', () => {
    expect(from_string_to_seconds("1")).toBe(1)
    expect(from_string_to_seconds("0:01")).toBe(1)
    expect(from_string_to_seconds("0:00:01")).toBe(1)
  })

  test('1 minute long, various formats', () => {
    expect(from_string_to_seconds("60")).toBe(60)
    expect(from_string_to_seconds("1:00")).toBe(60)
    expect(from_string_to_seconds("01:00")).toBe(60)
    expect(from_string_to_seconds("0:01:00")).toBe(60)
  })

  test('1 hour long, various formats', () => {
    expect(from_string_to_seconds("3600")).toBe(3600)
    expect(from_string_to_seconds("60:00")).toBe(3600)
    expect(from_string_to_seconds("59:60")).toBe(3600)
    expect(from_string_to_seconds("1:00:00")).toBe(3600)
  })

  test('various values', () => {
    expect(from_string_to_seconds("12:34")).toBe(754)
    expect(from_string_to_seconds("1:23:45")).toBe(5025)
    expect(from_string_to_seconds("100:00:59")).toBe(360059)
  })
})

describe('track length formating (output)', () => {
  test('1 second long', () => {
    expect(from_seconds_to_string(1)).toBe("0:01")
  })

  test('1 minute long', () => {
    expect(from_seconds_to_string(60)).toBe("1:00")
  })

  test('1 hour long', () => {
    expect(from_seconds_to_string(3600)).toBe("1:00:00")
  })

  test('various values', () => {
    expect(from_seconds_to_string(754)).toBe("12:34")
    expect(from_seconds_to_string(5025)).toBe("1:23:45")
    expect(from_seconds_to_string(360059)).toBe("100:00:59")
  })
})
